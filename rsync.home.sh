#!/bin/bash

# Be sure to change /path/to/rsync_ignore and /path/to/external/drive/destination/ to the appropriate directories.

rsync -avxP --exclude-from=/path/to/rsync_ignore --delete /home/$USER/ /path/to/external/drive/destination/
